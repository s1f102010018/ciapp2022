package com.example.ciapp2022.sample2;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Before;
import org.junit.Test;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1A", "Password1B");
    }

    @Test
    public void testLoginSuccess() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("Testuser1A", "Password1B");
        assertThat(user.getUsername(), is("Testuser1A"));
        assertThat(user.getPassword(), is("Password1B"));
    }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("Testuser1A", "Password1A");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUserNotFound() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("Testuser1B", "Password1B");
    }
}